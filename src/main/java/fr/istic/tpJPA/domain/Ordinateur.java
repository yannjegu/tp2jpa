package fr.istic.tpJPA.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Ordinateur {

	private Long id;
	
	private String nom;
	
	private List<Logiciel> logiciels = new ArrayList<Logiciel>();

	private List<Personne> personnes = new ArrayList<Personne>();

	public Ordinateur() {
		super();
	}

	public Ordinateur(String nom) {
		this.nom = nom;
	}
	
	public Ordinateur(String nom, List<Logiciel> logiciels, List<Personne> personnes) {
		this.nom = nom;
		this.logiciels = logiciels;
		this.personnes = personnes;
	}

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@OneToMany(mappedBy = "ordinateur", cascade = CascadeType.PERSIST)
	public List<Personne> getPersonnes() {
		return personnes;
	}

	public void setPersonnes(List<Personne> personnes) {
		this.personnes = personnes;
	}
	
	@OneToMany(mappedBy = "ordinateur", cascade = CascadeType.PERSIST)
	public List<Logiciel> getLogiciels() {
		return logiciels;
	}

	public void setLogiciels(List<Logiciel> logiciels) {
		this.logiciels = logiciels;
	}
}
