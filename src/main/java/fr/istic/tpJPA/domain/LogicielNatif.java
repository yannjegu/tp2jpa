package fr.istic.tpJPA.domain;

import java.util.List;

public class LogicielNatif extends Logiciel {
	public LogicielNatif() {
		super();
	}
	
	public LogicielNatif(String nom, Ordinateur ordinateur, List<Logiciel> dependencies) {
		super(nom, ordinateur, dependencies);
	}
}
