package fr.istic.tpJPA.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

public abstract class Logiciel {
	private Long id;
	private String nom;
	private Ordinateur ordinateur;
	private List<Logiciel> dependencies = new ArrayList<Logiciel>();
	
	public Logiciel() {}

	public Logiciel(String nom, Ordinateur ordinateur, List<Logiciel> dependencies) {
		this.nom = nom;
		this.ordinateur = ordinateur;
		this.dependencies = dependencies;
	}
	
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@ManyToOne
	public Ordinateur getOrdinateur() {
		return ordinateur;
	}

	public void setOrdinateur(Ordinateur ordinateur) {
		this.ordinateur = ordinateur;
	}
	
	@OneToMany
	@JoinColumn(name="dependency_id")
	public List<Logiciel> getDependencies() {
		return dependencies;
	}

	public void setDependencies(List<Logiciel> dependencies) {
		this.dependencies = dependencies;
	}

	@Override
	public String toString() {
		return "[id=" + this.id + ", nom=" + this.nom + ", ordinateur=" + this.ordinateur.getNom() + "]";
	}
	
}
