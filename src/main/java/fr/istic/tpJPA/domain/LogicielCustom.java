package fr.istic.tpJPA.domain;

import java.util.List;

import javax.persistence.Entity;

@Entity
public class LogicielCustom extends Logiciel {
	private String installationDate;
	
	public LogicielCustom() {
		super();
	}
	
	public LogicielCustom(String nom, String installationDate, Ordinateur ordinateur, List<Logiciel> dependencies) {
		super(nom, ordinateur, dependencies);
		this.installationDate = installationDate;
	}

	public String getInstallationDate() {
		return installationDate;
	}

	public void setInstallationDate(String installationDate) {
		this.installationDate = installationDate;
	}
}
