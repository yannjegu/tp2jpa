package fr.istic.tpJPA.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Personne {
	private Long id;

	private String nom;
	
	private String prenom;
	
	private String mail;
	
	private String telephone;

	private Ordinateur ordinateur;

	public Personne() {}
	
	public Personne(String nom, String prenom, String mail, String telephone, Ordinateur ordinateur) {
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.telephone = telephone;
		this.ordinateur = ordinateur;
	}

	public Personne(String nom, Ordinateur ordinateur) {
		this.nom = nom;
		this.ordinateur = ordinateur;
	}

	public Personne(String nom) {
		this.nom = nom;
	}

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@ManyToOne
	public Ordinateur getOrdinateur() {
		return ordinateur;
	}

	public void setOrdinateur(Ordinateur ordinateur) {
		this.ordinateur = ordinateur;
	}

	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", ordinateur="
				+ ordinateur.getNom() + "]";
	}

}