package fr.istic.tpJPA.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.istic.tpJPA.domain.Logiciel;
import fr.istic.tpJPA.domain.LogicielCustom;
import fr.istic.tpJPA.domain.LogicielNatif;
import fr.istic.tpJPA.domain.Ordinateur;
import fr.istic.tpJPA.domain.Personne;

public class JpaTest {

	private EntityManager manager;

	public JpaTest(EntityManager manager) {
		this.manager = manager;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory factory =   
 			 Persistence.createEntityManagerFactory("mysql");
		EntityManager manager = factory.createEntityManager();
		JpaTest test = new JpaTest(manager);

		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		
		try {
			test.createTuples();
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();

		test.listPersonnes();
		test.listLogiciels();
   	 	
   	 	manager.close();
		System.out.println(".. done");
	}

	private void createTuples() {
		List<Logiciel> eclipseDep = new ArrayList<Logiciel>();
		int numOfPersonnes = manager.createQuery("Select a From Personne a", Personne.class).getResultList().size();
		if (numOfPersonnes == 0) {
			Ordinateur o1 = new Ordinateur("Lenovo");
			Ordinateur o2 = new Ordinateur("HP");
			Ordinateur o3 = new Ordinateur("Sony");
			
			Logiciel l1 = new LogicielCustom("Spotify", "09/09/2016", o1, null);
			Logiciel l2 = new LogicielCustom("iTunes", "09/09/2016", o2, null);
			Logiciel l3 = new LogicielCustom("Sublime Text", "09/09/2016", o3, null);
			Logiciel l4 = new LogicielNatif("Java JDK 1.8", o1, null);
			
			eclipseDep.add(l4);
			
			Logiciel l5 = new LogicielCustom("Eclipse Neo", "09/09/2016", o1, eclipseDep);
			
			Personne p1 = new Personne("Capt'ain Guillou", o2);
			Personne p2 = new Personne("Ronan Le Cador", o3);
			Personne p3 = new Personne("Don Jegu", o1);
			manager.persist(o1);
			manager.persist(o2);
			manager.persist(o3);

			manager.persist(p1);
			manager.persist(p2);
			manager.persist(p3);
			
			manager.persist(l1);
			manager.persist(l2);
			manager.persist(l3);
			manager.persist(l4);
			manager.persist(l5);
		}
	}

	private void listPersonnes() {
		List<Personne> resultList = manager.createQuery("Select a From Personne a", Personne.class).getResultList();
		System.out.println("Nombre de personnes : " + resultList.size());
		for (Personne next : resultList) {
			System.out.println("personne : " + next);
		}
	}
	
	private void listLogiciels() {
		List<Logiciel> resultList = manager.createQuery("Select a From Logiciel a", Logiciel.class).getResultList();
		System.out.println("Nombre de logiciels:" + resultList.size());
		for (Logiciel next : resultList) {
			System.out.println("logiciel: " + next);
		}
	}
}